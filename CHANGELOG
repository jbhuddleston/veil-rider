12.078
- changed the field name for targets in Modifiers to "Target1, Target2, ..." 
to help people use them as intended.
- changed the behaviour of Container items so they do not automatically drop 
their items when added to an actor but show the Container sheet as with other 
items. They still remove their items when deleted though. A couple of 
"buttons" have been added to the Container sheet, so that you may deploy and 
remove their items from their actor. Be careful not to deploy them more than 
once or you will orphan previously dropped items; although that is a sneaky 
way to permanently add some items to your actor before removing the Container.
- you may now add Containers to other Containers. Be careful with this feature.
- added a per-client setting to delay the refresh of dialog windows so the 
update of data has time to take place on the actor.

12.077
- added a statusEffectChoice system setting that allows you to choose the 
foundry default status effects, the ones defined for the ruleset you are using 
by the system, or to define your own by creating a 'world' compendium for Items 
named 'Status Effects' and adding your Modifier items to it. They must have a 
name and an image and must be Always On because they will only be added to the 
token when the status is set and removed when it is unset. You can use either 
the token HUD to toggle your status effects or the new Token control Tool to 
'Assign Status Effects'. Click on the new tool icon to open a dialog with the 
status effects laid out in large format. Then select as many tokens as you like 
and toggle the effects you click on. Close the dialog when finished. If you 
have added modifiers to the Status Effects compendium with the same name as a 
Foundry or Ruleset Effect, your modifier item will be added and removed from 
the token as usual. Do have fun with this one.

12.076
- bugfix: toggling the "Mark Defeated" icon for a combatant will correctly 
mark them as defeated.

12.075
- fixed the damage chat card modifiers format and added the roll tooltip
- added a migration script to convert 'Damage Resistance' to 'Defensive Rating'
- changed rolls so that the default actor used is the one for the token 
selected, not just the actor template in the sidebar, unless the token selected 
doesn't match the original actor who made the roll that provided the link you 
are clicking.

12.074
- fixed the error where an actor is created with multiple copies of each item.
- fixed for the last time :-), the sequencing errors in item value calculation.

12.073
- added Melee Mod and Ranks: Baseline to core actor items
- fixed the damage rolls for those with modifiers.
- added MeleeCo: to Damage Mods

12.072
- updated the core actor items compendium, fixing withAdvantage, adding CQB and 
Marksmanship and filtering Full Auto and Aimed Shot better.
- removed all the template actors, replacing them with item containers
- reorganised skill containers
- added a damage chat card to hold draggable damage and healing
- fixed internal errors on item creation.
- fixed trait display to chat
- fixed header info errors that prevented compendium actors from opening (but 
they don't exist anymore) :-)
- fixed a sequencing issue that prevented modifiers using skills as references 
from obtaining the modified value of the skill.

12.071
- fixed Defence dropdown label.
- fixed modifier dropdown stuck on Attacks

12.070
- fixed the prosemirror editor

12.069
- fixed token resource bars

12.068
- Diceterm, getProperty, async rolls updated.

12.067
- initial v12 release ready for testing

0.66
- fixed broken modifiers for user rolls and reference values so they use the 
Skill category.
- added a macro to set the resources of selected tokens to bar1:hp and 
bar2:temphp and the visibility to always for owner. That should remind people 
to right click and modify HP, instead of opening their sheet.
- removed a couple of unwanted templates from compendia.

0.65
- properly supports open-ended d20 rolls with double 1's a critical failure. To 
roll one manually is [[1d20oe]].
- fixed a bunch of variable and modifier display issues where the first entry 
was being ignored.

0.64
- added Token Attacks, Defences, Skills and Spells macros that work with the 
new method but need tweaking. These allow you to select a token and bring up a 
list of choices for them to roll from.
- fixed the Token Initiative Roller that allows you to select a bunch of 
tokens and roll their initiative all at once.
- reorganised the system macros so you import them into a Core Macros folder 
instead of a folder within another folder.
- fixed the behaviour of the scrolls so that modifiers are fetched properly 
then rollable items are collected and modifiers are selected for presentation 
in a rollmacro dialog window.
- sidelined some of the old macro code and will be deleting it if not necessary 
later.

0.63
- bugfix: chat message template path fixed and a few other references to 
vsd changed to veil-rider.
- modifiers and variables inside containers have had the empty first entry 
removed.

0.62
- I forgot to relink the job journals to their items. Done.
- The macros may be old code. Will have to test them.
- the skill ranks containers all need to be rebuilt to get rid of the top blank 
entry in the variables. It can be done in the YAML much more easily than in 
Foundry.

0.61
- consolidated compendia
- first pass at upgrade, debugging to follow
- autopopulate actor with base items when created from the sidebar
- reconfigured modifiers and variables with no empty entries
- migration from v0.60- removes the empty top entry from existing vars and mods 
unless they are in a container.
- actor sheet appears to be displaying all items properly
- fixed pools dragging out of a container
- combat tracker fixes

0.60
- repaired a good number of svg images, adding a width and height value in 
pixels, so they will not break Firefox browsers.

0.59
- a little refactoring in the background
- added a MyDialog class to allow you to hit Enter when filling in fields in 
the dialog window to set their values rather than having to click outside the 
field to set it.
- added support for the pow(value, exponent) function in formulae.
- created better detection and prevention of dropping an item on the actor it 
came from (usually accidental).

0.58
- added a context menu option to clone an item
- bugfix: system compendia are now uncorrupted and so may open properly

0.57
- start of v11 dev

0.56
- opened the option for you to test a copy of your world in a Node.js server 
running FoundryVTT v11. Do not replace your v10 install with v11 yet and do 
not test it with the only copy of your world data. There is no going back 
from this migration of your data.
- I have done this and encountered no issues. I would like you to try and let 
me know if you have any.

0.55
- fixed up actorhooks
- rebuilt the skills compendium with Initial skill containers and Advancement 
skill ranks so you can drop them and not have to edit anything.
- bugfix: the template actors min health is fixed at -1/2 of max health. For 
any token actor sheet you open, if their min health is not negative then there 
is an underscore in the formula instead of a minus.

0.54
- upgraded containers to drag and drop with visual display of contents
- changed the item creation buttons and filters so they can be localised
- removed partial templates as they are no longer being used

0.53
- for items that send their notes to the chat, identify whether they are html 
and if not, add line breaks.

0.52
- bugfix: creating a formula with no valid reference no longer creates an 
endless loop of searching for it.
- added more information to error reporting and debugging of bad formulae.
- changed many debug and error messages to warnings

0.51
- bugfix: advantage will work now

0.50
- added a conditions compendium
- reworked the system macros
- added a temp hp mechanism
- created two template actors, one with all skills and one without
- converted Rollable notes to chat to Traits
- improved the character advancement section
- improved traits so they can store more info and sometimes use other types 
of items as traits instead of having two items.
- repaired the links in the character notes
- converted all item notes to plain text to speed up data entry
- added a display section for attributes
- redesigned the header section to present useful information clearly
- relabelled most of the system so it is clearer what things are used for
- created a migration script to handle almost all of the conversions and 
deletion of useless items
- added target information to attack rolls
- cleaned up a bunch of item types, removing some that are not applicable

0.32
- renamed many internal variables to be clearer and less system-specific
- simplified the localisation process but there is still work to do
- set it up so that only rollables which appear in a Ranks: variable are 
counted. Any other rollables may be defined manually. This enables the Variable 
Formula rollable to create user-defined rolls. Added a section on the skills 
tab to display them.
- bugfix: a number of items were incorrectly displaying fields.
- cleaned up a lot of files.

0.31
- added drag and drop behaviour for Variables and Modifiers (even between a 
Modifier and a Variable).

0.30
- first version compatible with Foundry VTT v10

0.23
- final version for FoundryVTT v9

0.22
- clean-up of the actor sheet. Removed many unecessary labels. Primary 
attributes are now only visible on All Items tab.
- changed the display of variables and ranks so you can see what is in them 
without opening them. This is not the final step, just an improvement.

0.21
- Re-enabled editing and deleting of items throughout the sheet.
- enabled initiative rolls to send chat messages
- removed skills from the actor template and added them to the compendium 
to be added at need.
- removed the post-processing step for reference calculation as it tends to 
double the effect of modifiers. Use Ranks: variables to permanently modify 
and skill that is used for attack rolls or as a reference for other skills.
- tidied up a bunch of excess files from the source systems

0.20
- bugfix: the rollmacro dialog will stay open for all rolls, not just attacks.
- combat initiative will now be rolled normally in the combat tracker, not on 
the sheet. The formula is "1d20 + @dynamic.initiative.moddedvalue" so you may 
still apply modifiers to the initiative roll through the Initiative item. The 
Initiative item should be changed to the category "Value to Chat" to prevent 
it from being rolled.
- cleaned up the values in the header of the character sheet as they are 
neither rollable nor directly editable.
- tuned the Aimed Shot and Full Auto modifiers to only affect Ranged skills 
and attacks. Updated in the VR Template actor and VR Template Container item.
- Updated the Scout Job

0.19
- roll macro dialogs will now reopen after rolling so you can do it again if 
you want.
- aimed shot and full auto have been updated in the vr template container and 
VR Items compendium and on the VR Template actor.
- Ammo pools now appear on the combat tab under their attacks if their name 
starts with "Ammo:"

0.18
- added the PETS skill to the VR Template actor
- added the rank for PETS to the PETS Skill ranks item

0.17
- added post-processing to allow skills used for combat to be modified as well.

0.16
- added PETS skill and skill ranks
- fixed Technician Lvl1 Job skills
- updated the VR Template actor.

0.15
- Completed items and editing for: Demolitions, Gambler, Gunner and Medic.

0.14
- Tidied the text on all Job Journals
- Completed the items and editing for: Chef, Scout, Radioman, Veil Auxiliary, 
Transporter and Technician.

0.13
- decorative: changed the token border thickness from 15 to 9 pixels
- changed the function of the weapon Crit Damage Multiplier field to hold the 
Ammo Pool Abbreviation.
- converted actor sheet rolls to rollmacros, bringing up a dialog with the 
appropriate modifiers from which you may then make the roll.
- altered the attacks function so the message is dependent on the type of the 
attack and the damage will then be rolled from the chat. Attacks with ammo pools 
will decrement the ammo automatically, warn when reload is required and alert 
when an attack was attempted with no ammo.
- added the withAdvantage variable to every roll dialog.
- we are restricting the AP in combat to the APBase modded value.
- roll dialogs now close after rolling but not for a modlist check.
- pools now update their min and max formula calculations properly.
- Any notes to chat items now display a comment bubble instead of dice.
- updated the VR template container (in VR Items) with the new move mode and the 
Flying and Swimming items. Updated Race templates with either flying or swimming 
to populate those values.
- there are four job journals properly defined now: Battle Buddy, Battlefield 
Forensics, Brawler and Breacher. Any skill bonuses provided by a level in a job 
are now defined in a modifier so there is an audit trail of modifiers for 
applicable rolls.
- the weapons compendium items have been updated to work with ammo counting and 
any weapons equipment items have been removed. The weapon rules text has been 
included with the attacks instead.
- extra template actors have been removed from the compendium, leaving only the 
working VR Template compendium actor and the racial template actors. 

0.12
- changed the sort order to sort by name. May have to hunt down code 
allowing drag to resort.
- adding examples of each type of weapon.
- changed Pistol and Sniper Rifle skill names to plural to match all the 
others. Changes made in VR template and container and skill ranks items.
- the weapons are listed one item at a time so make sure you copy all the 
bits you need for each one. eg. The "Firearm: FAL", "Ammo: FAL", and 
"FAL Normal Shot" are all pieces you will need for that weapon.

0.11
- added a system compendium for job-related items.
- completed the job journals for Battle Buddy, Battlefield Forensics and 
Brawler.
- fixed the edit function for attacks.
- positioned the WithAdvantage and Range variables in default locations.
- fixed the @throwing reference to @throwables for handaxe and knife
- updated the Kharnak sample actor.

0.10
- changed function so that clicking on the name of an item on the actor 
sheet will bring up it's editor on any tab on the sheet. You need to be on 
the all items tab and right-click on the name to bring up the context menu 
where you may delete something.
- changed function so that clicking on a number in [] brings up the modifier 
calculation dialog.
- notes to chat now have a scroll to click on.
- modifiers are toggled by clicking in the empty space to the right of the 
name.
- cleaned up the header and render items that are useful to the game. Not 
finished but in better shape.
- added a sample first level character "Kharnak" to the dropbox for testing 
and ideas. I will keep working on it as more of the game becomes clear to me.
- The VR Template actor and Container item have been updated.

0.09
- Added the Racial Check to the skills list in the VR Template along with 
the racial check mod.

0.08
- finished the racial actor templates and removing the formatting from the 
journals.

0.07
- added Centaurs and Staglings

0.06
- added min to the display of HP in the header.
- bugfix: initiative formula was accidentlaly deleted.
- added Halfling and Havedderr races
- Changed CQB expertise to just CQB
- added a few sample weapons and items to the compendium
- added the sample fighters to the actors compendium

0.05
- started work on race actors, cleaning up the journals along the way.
- Agordaeling, Brimtouched, Dwarf and Goliath.
- added the current version of the VR Template Container to the Items Compendium

0.04
- created compendia for all items, journals and actors in progress. Equipment, 
Weapons, Jobs, Races, Actors and random items.
- choosing to go with actor race templates instead of containers because of 
ease of maintenance.
- updated the readme

0.03
- changed most notes fields to rich text editors instead of textarea.
- added the initial Job Journals compendium, just text, no formatting.
- think I fixed Modifiers so if you set them to perm, they are properly in 
effect as well, even if you create them on the sidebar.

0.02
- Tidy Up, removing unnecessary things

0.01
- Initial Commit

#####################################################
2.21
- added VR Template Actor, a Job Item and a Job Journal to their respective 
dropboxes.
- added Veil Rider Races and Skills compendia to the system.
- included Primary Attributes when counting ranks is turned on.
- bugfix: including Variable Formulae in the all items tab now

2.20
- Added a new combat tracker for the Veil Rider TTRPG. It allows you to set an 
initial number of action points, then a different number to be added each round. 
You may spend as many as you like on your turn using the + and - buttons. It is 
fully compatible with the "Combat Enhancements" module and allows you to use the 
radial health bar and show the HP editor. You can see up to 6 status effects 
without the editor or 4 with it.
- bugfix: the status effect size selection only worked when you set it until 
your first refresh.
- bugfix: on the Trooper sheet on the All Items tab, the skills section 
was named incorrectly as Variable Formula.
