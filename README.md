# Veil Rider

Veil Rider TTRPG

## Description

This system is a game aid for playing the Veil Rider TTRPG on Foundry VTT, using it's Manuals. The Veil Riders is a book series from the author Guardbro, written for those who want to be an Elf with a machine gun or a Goliath with a rocket launcher.

The manuals can be found below via a google doc link:

 https://docs.google.com/document/d/1nNtVCHtWZYL_xqCqJ_hqA4dGXvQGADSyTI3g7Rh66O4/edit

Streaming of gameplay can be found here:

 https://www.youtube.com/c/GuardbeardiaBeardio

The material presented here is my original creation, intended for use with the Veil Rider TTRPG system from Guardbro and Pirate0Potato. This material is official and is endorsed by the author Guardbro.

The Veil Rider TTRPG is an original creation of Guardbro and the Guardbro's Field Desk channel (https://www.youtube.com/channel/UCP2HuOCArqZtyqrndWtF6Dw), and its rules and art are copyrighted by Guardbro and their creators. All rights are reserved by Guardbro and Dart Frog Books. This game aid is the original creation of James Huddleston (https://www.youtube.com/channel/UC-WeR6LCq_UuPpldldbGDvw), and is released for free distribution, and not for resale, under the permissions granted by Guardbro.
