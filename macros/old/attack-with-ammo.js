main()
async function main() {
    //Enter the exact name of the attack (or weapon) from the character sheet in quotes in the line below - copy/paste it if you have to
    const attackname = "Firearm: P226";
    //use the abbreviation for the pool you want to decrement for the attack
    const ammoname = "AmmoP226";

    // ensure one token is selected, then get it's actor
    if (canvas.tokens.controlled.length != 1) {
        ui.notifications.error("Please select one token");
        return;
    }
    const actor = canvas.tokens.controlled[0].actor;

    // Get selected target info
    let targets = Array.from(game.user.targets)
    if (targets.length != 1) {
        ui.notifications.error("Please target one token");
        return;
    }
    const target = targets[0].actor;

    const ammo = actor.system.tracked[ammoname.toLowerCase()];
    console.log(ammo);
    
    //Gather the attack info from the actor sheet to populate the chat and roll data
    const itemToGet = actor.items.getName(attackname)
    if (!itemToGet) {
        ui.notifications.error(`Can't find '${attackname}'. Please check the attack name carefully.`);
        return;
    }
    const itemDmg = itemToGet.system.damage;

    //get the Rate of Fire(RoF) value, which is coded in data.accuracy
    const itemRoF = itemToGet.system.accuracy;
    console.log(itemRoF);

    // Decrement Pool by the Rate of Fire value
    await actor.updateEmbeddedDocuments("Item", [
        { _id: ammo._id, data: { value: ammo.value - itemRoF } }
    ]);

    // Get remaining ammo count post-attack to display in output
    const ammo_left = actor.system.tracked[ammoname.toLowerCase()].value;
    console.log(ammo_left);

    // Get attack range info for display in output
    const itemRange = itemToGet.system.range;
    //Construct the chat message flavor text that will be passed to the roll data
    let resultsHtml = `
<h2>${attackname}</h2> ${actor.name} fires at ${target.name} with their ${attackname}<br><br>
<b>Range:</b> ${itemRange}<br>
<b>Ammo used:</b> ${itemRoF} (Remaining: ${ammo_left})<br>
<b>Damage:</b><span class="rollable" data-name="${attackname}" data-roll="${itemDmg}" data-type="damage"> ${itemDmg}</span><br>
<b>Strike Modifiers:</b><p style="font-size:0.75vw;">Base:  `


    //Define the roll data
    //define the critical hit die roll threshold in the 'threat' value below - currently a natural 20.
    //define any additional base modifiers in the 'roll' value below that are not already defined on the character sheet - typically not needed if you already have permanent, temporary, or some other situational modifiers enabled on the character sheet - thus, currently set to 0
    const rolldata = {
        actor: actor,
        flavour: resultsHtml,
        threat: 20,
        type: "attack",
        modtype: "",
        roll: 0,
        name: attackname,
    }

    //Roll and create the chat message
    actor.roll(rolldata);
}