main()

async function main() {
  const folderName = "Initial Skill Selection";

  const folderId = game.collections.get("Folder").getName(folderName).id;
  console.log("Folder ID: ", folderId);
  const items = game.collections.get("Item").filter(function (item) {
    return item.folder?.id == folderId;
  });
  console.log("Folder Contents: ", items);
  items.forEach(async function (item) {
    if (item.img == "icons/svg/mystery-man-black.svg")
      item.update({ img: "icons/skills/trades/smithing-anvil-silver-red.webp" });
  });
}