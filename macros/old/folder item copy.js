main()

async function main() {
    const folderName = "Conditions";

    if (canvas.tokens.controlled.length == 0) {
        ui.notifications.error("Please select at least one token first");
        return;
    }

    const folderId = game.collections.get("Folder").getName(folderName).id;
    const items = game.collections.get("Item").filter(function (item) {
        return item.folder?.id == folderId;
    });
    console.log("Folder Contents: ", items);

    canvas.tokens.controlled.forEach( async function (token) {
        const actor = token.actor;
        const newitems = [];
        const olditems = [];
        items.forEach(function (item) {
            const itemdata = item.system;
            // set the chartype to the type of the actor
            itemdata.chartype = actor.type;
            // add the item to the array of those to be added to the actor
            newitems.push(itemdata);
            // if there is already an item of this name and type, delete it
            const existing = actor.items.filter(item => item.name == item.name);
            while (existing[0]) {
                const current = existing.pop();
                if (current.type == item.type) olditems.push(current.id)
            }
        });
        await actor.deleteEmbeddedDocuments('Item', olditems);
        await actor.createEmbeddedDocuments('Item', newitems, { renderSheet: false });
    });


}