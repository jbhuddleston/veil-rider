import { system } from "../config.js";
import { BaseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {BaseActorSheet}
 */
export class ActorVeilRiderSheet extends BaseActorSheet {
  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["veil-rider", "sheet", "actor"],
      template: "systems/veil-rider/templates/actor/actorVeilRider-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [
        {
          navSelector: ".sheet-nav",
          contentSelector: ".sheet-body",
          initial: "combat",
        },
      ],
    });
  }

  /** @override */
  async getData(options) {
    if (CONFIG.system.testMode)
      console.debug("entering getData() in actorVeilRider-sheet");

    const context = await super.getData(options);
    const actor = context.actor;
    const dynamic = actor.system.dynamic;
    const tracked = actor.system.tracked;
    const displaydata = actor.system.sheetdata;
    const headinfo = displaydata.headerinfo = {};

    const emptyitem = {
      _id: "",
      system: {
        value: 0,
        moddedvalue: 0,
        max: 0,
        abbr: "",
        label: "",
        entries: []
      }
    };
    headinfo.level = dynamic.level || emptyitem;
    headinfo.dr = dynamic.damage_resistance || emptyitem;
    headinfo.hitpoints = tracked.hp || emptyitem;
    headinfo.apr = dynamic.apr || emptyitem;
    headinfo.pace = dynamic.move_mode || emptyitem;
    /*
        for (let item of displaydata.traits) {
          switch (item?.name) {
            case "Race":
              headinfo.race = item;
              break;
            case "Character Class":
              headinfo.characterclass = item;
              break;
            case "Specific Information":
              headinfo.specificinfo = item;
              break;
            case "Additional Information":
              headinfo.additionalinfo = item;
              break;
          }
        }
    */
    // group and sort rankitems correctly for display
    const rid = {};
    for (const item of displaydata.rankitems) {
      const group = item.system.group;
      item.system.displayname = item.name.split("nks:")[1].trim();
      if (!rid[group]) {
        rid[group] = {
          name: group,
          items: []
        };
      }
      rid[group].items.push(item);
    }
    displaydata.rankitemsdisplay = this.sort(Object.values(rid));

    // group and sort skills correctly for display
    const tempskills = [...displaydata.skills];
    const tempspells = [...displaydata.spells];
    displaydata.allskills = [];
    displaydata.skills = [];
    displaydata.techniques = [];
    for (const item of tempskills) {
      if (item.system.category == "technique") {
        displaydata.techniques.push(item);
      } else {
        item.system.stepvalue = item.system.moddedvalue - item.system.value;
        displaydata.skills.push(item);
        displaydata.allskills.push(item);
      }
    }
    displaydata.spells = [];
    displaydata.rms = [];
    for (let item of tempspells) {
      if (item.system.category == "rms") {
        displaydata.rms.push(item);
      } else {
        displaydata.spells.push(item);
        displaydata.allskills.push(item);
      }
    }
    for (let item of displaydata.checks) {
      displaydata.allskills.push(item);
    }
    displaydata.allskills = this.sort(displaydata.allskills);

    // group and sort defences correctly for display
    let tempdefences = [...displaydata.defences];
    displaydata.defences = [];
    displaydata.blocks = [];
    displaydata.parrys = [];
    displaydata.dodges = [];
    for (let item of tempdefences) {
      if (item.system.category == "block") {
        displaydata.blocks.push(item);
      } else {
        item.system.stepvalue = item.system.moddedvalue - item.system.value;
        displaydata.defences.push(item);
        if (item.system.category == "dodge") {
          displaydata.dodges.push(item);
        } else {
          displaydata.parrys.push(item);
        }
      }
    }
    displaydata.attackvariables = [];
    for (let varname of system.variables.attackvariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname])
        displaydata.attackvariables.push(dynamic[varname]);
      else if (tracked[varname])
        displaydata.attackvariables.push(tracked[varname]);
    }
    for (let pooldata of displaydata.pools) {
      if (pooldata.name.startsWith("Ammo:")) displaydata.attackvariables.push(pooldata);
    }
    displaydata.defencevariables = [];
    for (let varname of system.variables.defencevariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname])
        displaydata.defencevariables.push(dynamic[varname]);
      else if (tracked[varname])
        displaydata.defencevariables.push(tracked[varname]);
    }
    displaydata.skillvariables = [];
    for (let varname of system.variables.skillvariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname])
        displaydata.skillvariables.push(dynamic[varname]);
      else if (tracked[varname])
        displaydata.skillvariables.push(tracked[varname]);
    }
    displaydata.spellvariables = [];
    for (let varname of system.variables.spellvariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname])
        displaydata.spellvariables.push(dynamic[varname]);
      else if (tracked[varname])
        displaydata.spellvariables.push(tracked[varname]);
    }

    // group and sort skill mods
    displaydata.checkskillspellmods = this.sort(
      Array.from(new Set(displaydata.checkmods.concat(displaydata.skillmods, displaydata.spellmods)))
    );

    delete displaydata.skillmods;
    delete displaydata.checkmods;
    delete displaydata.spellmods;

    return context;
  }

  sort(displaydata) {
    return displaydata.sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });
  }

  async _onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    const token = game.canvas.tokens.controlled[0] || false;
    const actor = token ? token.actor._id == this.actor._id ? token.actor : this.actor : this.actor;
    let flavour = game.i18n.format(`local.phrases.${dataset.type}`, { name: dataset.name, }) || "";
    if (dataset.type == "attack") {
      const attack = this.actor.items.get(dataset.itemId);
      const attackdata = attack.system;
      dataset.damagetype = attackdata.damageType;
      const ammopool = this.actor.system.tracked[attackdata.minST.toLowerCase()];
      if (ammopool) {
        // has ammunition
        let ammoused = Math.min(attackdata.accuracy, ammopool.system.value);
        // Decrement Pool by the Rate of Fire value
        await this.actor.updateEmbeddedDocuments("Item", [{ _id: ammopool._id, system: { value: Math.max(0, ammopool.system.value - ammoused) }, },]);
        // Get remaining ammo count post-attack to display in output
        const ammo_left = ammopool.system.value;
        flavour = `Attacks <b class="critfail">${dataset.target}</b> with their:</br><b>${attack.name}</b><hr>
        <b>Range:</b> ${attackdata.range}<br>`;
        if (ammoused == 0) {
          flavour += `<b>Ammo used:</b> ${ammoused} <span class="critfail">Click! Attack failed! Reload!</span><br>`;
        } else if (ammo_left == 0) {
          flavour += `<b>Ammo used:</b> ${ammoused} (Remaining: ${ammo_left}) <span class="critfail">Reload!</span><br>`;
        } else {
          flavour += `<b>Ammo used:</b> ${ammoused} (Remaining: ${ammo_left})<br>`;
        }
        flavour += `<b>Damage:</b><span class="rollable" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage" data-damagetype="${attackdata.damageType}"> ${attackdata.damage} - ${attackdata.damageType}</span><hr>
        <b>Strike Modifiers:</b><br>Base:  `;
      } else {
        if (attack.type == "Ranged-Attack") {
          flavour = `Attacks with their <b>${attack.name}</b><hr>
          <b>Range:</b> ${attackdata.range}<br>
          <b>Damage:</b><span class="rollable" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage" data-damagetype="${attackdata.damageType}"> ${attackdata.damage} - ${attackdata.damageType}</span><hr>
          <b>Strike Modifiers:</b><br>Base:  `;
        } else {
          // a melee attack
          flavour = `Attacks with their <b>${attack.name}</b><hr>
          <b>Damage:</b><span class="rollable" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage" data-damagetype="${attackdata.damageType}"> ${attackdata.damage} - ${attackdata.damageType}</span><hr>
          <b>Strike Modifiers:</b><br>Base:  `;
        }
      }
    }

    const rolldata = {
      actor: actor,
      flavour: flavour,
      threat: Number(dataset.threat) || 20,
      type: dataset.type || "",
      damagetype: dataset.damagetype || "",
      modtype: dataset.modtype || "",
      roll: dataset.roll || "",
      name: dataset.name || "",
    };

    actor.roll(rolldata);
    return;
  }
}
