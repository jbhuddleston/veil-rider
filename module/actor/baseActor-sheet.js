import { system } from "../config.js";
/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class BaseActorSheet extends ActorSheet {
  itemContextMenu = [
    {
      name: game.i18n.localize("local.sheet.clone"),
      icon: '<i class="fas fa-copy"></i>',
      callback: (element) => {
        const item = foundry.utils.duplicate(this.actor.items.get(element.data("item-id")));
        delete item._id;
        delete item.sort;
        if (item.flags) delete item.flags;
        if (item.effects) delete item.effects;
        this.actor.createEmbeddedDocuments("Item", [item], {
          renderSheet: true,
        });
      },
    },
    {
      name: game.i18n.localize("local.sheet.edit"),
      icon: '<i class="fas fa-edit"></i>',
      callback: (element) => {
        const item = this.actor.items.get(element.data("item-id"));
        item.sheet.render(true);
      }
    },
    {
      name: game.i18n.localize("local.sheet.delete"),
      icon: '<i class="fas fa-trash"></i>',
      callback: (element) => {
        this.deleteItem(element.data("item-id"));
      }
    }
  ];

  /** @override */
  async getData() {
    if (CONFIG.system.testMode)
      console.debug("entering getData() in baseActor-sheet\n", this);

    const context = super.getData();
    const actor = context.actor;
    context.system = actor.system;
    context.items = context.items.sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });
    context.enrichment = {
      system: {
        biography: await TextEditor.enrichHTML(actor.system.biography)
      }
    };

    // My shortcuts
    const dynamic = actor.system.dynamic;
    const tracked = actor.system.tracked;
    const displaydata = actor.system.sheetdata;

    displaydata.primaryattributes = [];
    displaydata.Equipment = [];
    displaydata.skills = [];
    displaydata.spells = [];
    displaydata.checks = [];
    displaydata.traits = [];
    displaydata.advantages = [];
    displaydata.disadvantages = [];
    displaydata.perks = [];
    displaydata.quirks = [];
    displaydata.attacks = [];
    displaydata.defences = [];
    displaydata.pools = [];
    displaydata.containers = [];
    displaydata.variables = [];
    displaydata.attackdamagemods = [];
    displaydata.defencemods = [];
    displaydata.reactionmods = [];
    displaydata.skillmods = [];
    displaydata.spellmods = [];
    displaydata.checkmods = [];
    displaydata.primods = [];

    for (const item of context.items) {
      const itemdata = item.system;
      switch (item.type) {
        case "Primary-Attribute": {
          displaydata.primaryattributes.push(item);
          break;
        }
        case "Equipment": {
          displaydata.Equipment.push(item);
          break;
        }
        case "Rollable": {
          itemdata.displaycategory = game.i18n.localize(`local.item.Rollable.${itemdata.category}`);
          switch (itemdata.category) {
            case "skill":
            case "technique": {
              displaydata.skills.push(item);
              break;
            }
            case "spell":
            case "rms": {
              displaydata.spells.push(item);
              break;
            }
            case "check": {
              displaydata.checks.push(item);
              break;
            }
          }
          break;
        }
        case "Power": {
          displaydata.skills.push(item);
          break;
        }
        case "Trait": {
          displaydata.traits.push(item);
          itemdata.displaycategory = game.i18n.localize(`local.item.Trait.${itemdata.category}`);
          if (itemdata.group.split(" ")[0].toLowerCase() != "notrait") {
            switch (itemdata.category) {
              case "advantage": {
                displaydata.advantages.push(item);
                break;
              }
              case "disadvantage": {
                displaydata.disadvantages.push(item);
                break;
              }
              case "perk": {
                displaydata.perks.push(item);
                break;
              }
              case "quirk": {
                displaydata.quirks.push(item);
                break;
              }
            }
          }
          break;
        }
        case "Ranged-Attack":
        case "Melee-Attack": {
          displaydata.attacks.push(item);
          break;
        }
        case "Defence": {
          displaydata.defences.push(item);
          break;
        }
        case "Pool": {
          displaydata.pools.push(item);
          break;
        }
        case "Container": {
          displaydata.containers.push(item);
          break;
        }
        case "Variable": {
          displaydata.variables.push(item);
          break;
        }
        case "Modifier": {
          if (itemdata.attack || itemdata.damage) displaydata.attackdamagemods.push(item);
          if (itemdata.defence) displaydata.defencemods.push(item);
          if (itemdata.reaction) displaydata.reactionmods.push(item);
          if (itemdata.skill) displaydata.skillmods.push(item);
          if (itemdata.spell) displaydata.spellmods.push(item);
          if (itemdata.check) displaydata.checkmods.push(item);
          if (itemdata.primary) displaydata.primods.push(item);
          break;
        }
      }
      if (item.type != "Trait") {// For non-trait items to be displayed with them
        // add the item to the trait list
        switch (itemdata.group.split(" ")[0].toLowerCase()) {
          case "spell":
          case "advantage": {
            displaydata.advantages.push(item);
            break;
          }
          case "race":
          case "disadvantage": {
            displaydata.disadvantages.push(item);
            break;
          }
          case "character":
          case "perk": {
            displaydata.perks.push(item);
            break;
          }
          case "job":
          case "quirk": {
            displaydata.quirks.push(item);
            break;
          }
        }
      }
    }

    // filter items in All Items
    displaydata.allitemstemp = [];
    let filterstring = "";
    for (const filter of Object.entries(actor.system.allitems)) {
      const itemtype = {
        name: filter[0],
        filtered: filter[1],
        localname: game.i18n.localize(`local.item.${filter[0]}.itemname`)
      }
      displaydata.allitemstemp.push(itemtype);
      if (filter[1]) filterstring += filter[0];
    }
    if (filterstring == "") {
      context.filtereditems = context.items;
    } else {
      context.filtereditems = context.items.filter(function (item) {
        if (filterstring.includes(item.type)) return item;
        return null;
      });
    }

    return context;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find(".item-create").click(this._onItemCreate.bind(this));
    html.find(".item-export").click(this._onItemExport.bind(this));
    html.find(".importJSON").click(this._onImportJSON.bind(this));

    html.find(".iteminput").change(this._onInputChange.bind(this));
    html.find(".plus").click(this._onPlusMinus.bind(this));
    html.find(".minus").click(this._onPlusMinus.bind(this));
    html.find(".rollable").click(this._onRoll.bind(this));
    html.find(".rollmacro").click(this._rollmacro.bind(this));
    html.find(".item-edit").click(this._onItemEdit.bind(this));
    html.find(".item-delete").click(this._onItemDelete.bind(this));
    html.find(".item-toggle").click(this._onToggleItem.bind(this));
    html.find(".collapsible").click(this._onToggleCollapse.bind(this));

    // for a checkbox representation of a pool
    html.on("click", ".click-pool", async (ev) => {
      ev.preventDefault();
      const target = ev.currentTarget;
      await this.actor.modifyTokenAttribute(
        `tracked.${target.dataset.abbr.toLowerCase()}`,
        target.dataset.value,
        false,
        true
      );
    });
    // not a contextmenu but just a right-click
    html.on("contextmenu", ".click-pool", async (ev) => {
      ev.preventDefault();
      const target = ev.currentTarget;
      await this.actor.modifyTokenAttribute(
        `tracked.${target.dataset.abbr.toLowerCase()}`,
        target.dataset.value - 1,
        false,
        true
      );
    });
    // for a checkbox representation of a pool
    html.on("click", ".click-value", async (ev) => {
      ev.preventDefault();
      const target = ev.currentTarget;
      const targetitem = target.parentElement;
      // get the item id
      const itemId = targetitem.dataset.itemId;
      // get the name of the changed element
      const dataname = targetitem.dataset.name;
      // get the new value
      const value = target.dataset.value;
      // Get the original item.
      const item = this.actor.items.get(itemId);
      // update the item with the new value for the element or the actor if it is gmod
      await item.update({ [dataname]: Number(value) });
    });
    // not a contextmenu but just a right-click
    html.on("contextmenu", ".click-value", async (ev) => {
      ev.preventDefault();
      const target = ev.currentTarget;
      const targetitem = target.parentElement;
      // get the item id
      const itemId = targetitem.dataset.itemId;
      // get the name of the changed element
      const dataname = targetitem.dataset.name;
      // get the new value
      const value = target.dataset.value - 1;
      // Get the original item.
      const item = this.actor.items.get(itemId);
      // update the item with the new value for the element or the actor if it is gmod
      await item.update({ [dataname]: Number(value) });
    });

    new ContextMenu(html, ".item", this.itemContextMenu);

    const handler = (ev) => this._onDragStart(ev);
    html.find(".draggable").each((i, li) => {
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, false);
    });

    html.on('click', '.filter', ev => {
      const type = ev.currentTarget.dataset.type;
      const current = this.actor.system.allitems[type];
      const update = `system.allitems.${type}`;
      this.actor.update({ [update]: !current });
    });

    html.on("click", ".open-journal", async (ev) => {
      system.showJournal(ev.currentTarget.dataset.table);
    });
  }

  /**
   * Each field is a comma-separated list of the things you want
   * to include in the result. It matches against the text you
   * enter, so a category of "s" will match skill, spell and rms.
   *
   * type: may be any of {Primary, Melee, Ranged, 
   * Attack, Rollable, Defence}
   *
   * category: applies only to:
   * Rollable: {check, skill, spell, technique, rms} and
   * Defence: {dodge, parry, block}
   * and may be used without specifying the type.
   *
   * group: is a user-defined field for use in filtering
   * this result.
   *
   * target: Any case-sensitive portion of the item name.
  */
  _rollmacro(ev) {
    const options = {
      position: {
        width: 300,
        top: 100,
        left: 100
      },
      type: ev.currentTarget.dataset.type || "",
      category: ev.currentTarget.dataset.category || "",
      group: ev.currentTarget.dataset.group || "",
      target: ev.currentTarget.dataset.target || "",
    };
    const tokens = game.canvas.tokens.controlled;
    if (tokens.length != 0) { // use the token's actor just in case they are not linked
      if (tokens[0].actor.id == this.actor.id) tokens[0].actor.rollables(options);
    } else {
      this.actor.rollables(options);
    }
  }

  async _onImportJSON(event) {
    event.preventDefault();
    let scriptdata = this.actor.system.itemscript || "";
    // preserve newlines, etc - use valid JSON
    scriptdata = scriptdata
      .replace(/\\n/g, "\\n")
      .replace(/\\'/g, "\\'")
      .replace(/\\"/g, '\\"')
      .replace(/\\&/g, "\\&")
      .replace(/\\r/g, "\\r")
      .replace(/\\t/g, "\\t")
      .replace(/\\b/g, "\\b")
      .replace(/\\f/g, "\\f");
    // remove non-printable and other non-valid JSON chars
    scriptdata = scriptdata.replace(/[\u0000-\u0019]+/g, "");
    const data = JSON.parse(scriptdata);
    console.log("JSON Object\n", data);
  }

  _onToggleCollapse(event) {
    event.preventDefault();
    if (CONFIG.system.testMode) console.debug("entering _onToggleCollapse()", [this, event]);
    // do not collapse when creating an item
    if (event.target.parentElement.className.includes("item-create")) return;
    if (event.target.parentElement.className.includes("char-name")) return;
    const section = event.currentTarget.dataset.section;
    const current = this.actor.system.sections[section];
    const update = `system.sections.${section}`;
    const data = current === "block" ? "none" : "block";
    this.actor.update({ [update]: data });
  }

  async _onPlusMinus(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onPlusMinus()", [this, event]);

    const field = event.currentTarget.firstElementChild;
    const fieldName = field.name;
    const change = Number(field.value);
    var value;
    var fieldValue;

    if (field.className.includes("pool")) {
      return this.actor.modifyTokenAttribute(`tracked.${fieldName.toLowerCase()}`, field.value, true, true);
    } else {
      // haven't figured out non-pools yet

      switch (fieldName) {
        case "gmod": {
          fieldValue = "system.gmod.value";
          value = change + this.actor.system.gmod.value;
          this.actor.update({ [fieldValue]: value });
          break;
        }
        default: {
          break;
        }
      }
    }
  }

  _onToggleItem(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    let attr = "";
    switch (item.type) {
      case "Modifier": {
        attr = "system.inEffect";
        break;
      }
      default: {
        ui.notifications.warn(`Toggling of ${item.type} is not yet supported.`)
      }
    }
    return this.actor.updateEmbeddedDocuments("Item", [{ _id: itemId, [attr]: !foundry.utils.getProperty(item, attr) }]);
  }

  _onItemEdit(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    item.sheet.render(true);
  }

  deleteItem(itemId) {
    const item = this.actor.items.get(itemId);
    const items = [itemId];
    if (item.type == "Container") {
      if (item.system.dropped)
        for (let dropped of item.system.dropped) {
          if (this.actor.items.get(dropped))
            items.push(dropped);
        }
    }
    this.actor.deleteEmbeddedDocuments("Item", items);
  }

  _onItemDelete(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    this.deleteItem(itemId);
  }

  async dropItem(item) {

    // the item has already been created on the actor sheet
    const baseitem = await this.actor.items.get(item[0].id);

    // show the item sheet
    baseitem.sheet.render(true);
    return baseitem;
  }

  async dropData(dragItem, token) {
    if (CONFIG.system.testMode)
      console.debug(`processing ${dragItem.type}\n`, dragItem);
    let data = {};
    switch (dragItem.type) {
      case "critical": {
        if (dragItem.bleed != 0 || dragItem.action != 0) {
          // there is a wound to process
          let severity = "na";
          let dlth = 0;
          if (dragItem.action > 50) {
            severity = "crippling";
            dlth = 33;
          } else if (dragItem.action > 20) {
            severity = "serious";
            dlth = 13;
          } else if (dragItem.action > 0) {
            severity = "minor";
            dlth = 3;
          }
          data = {
            name: dragItem.woundtitle,
            type: "Hit-Location",
            system: {
              chartype: "CharacterVsD",
              damageResistance: Number(dragItem.bleed) || 0,
              damage: -Number(dragItem.action) || 0,
              injuryType: severity,
              toHitPenalty: dlth,
              notes: dragItem.message,
            },
          };
          if (dragItem.bleed != 0 && token != null) await this.setStatusEffect("bleeding", token);
          const item = await this.actor.createEmbeddedDocuments("Item", [data]);
          const baseitem = await this.actor.items.get(item[0].id);
          baseitem.sheet.render(true);
        }
        if (dragItem.hits) {
          // apply damage so it will update status and bar
          await this.actor.modifyTokenAttribute("tracked.hp", -dragItem.hits, true);
        }
        let effectMsg = ".";
        if (dragItem.effect) {
          // apply an effect (TODO: make a longer list)
          if (dragItem.effect.includes("stun")) {
            if (token != null) await this.setStatusEffect("stunned", token);
            const item = await this.actor.getNamedItem("modifiers.stunned");
            effectMsg = ", and appears to have been <b>Stunned</b>.";
            await item.update({ "system.inEffect": true });
          }
          if (dragItem.effect.includes("soul-damage")) {
            const souldamage = Number(dragItem.effect.split("ul-damage[")[1].split("]")[0]);
            const item = await this.actor.getNamedItem("dynamic.sd");
            await item.update({ "system.attr": Number(item.system.attr) + souldamage });
            effectMsg = ", and appears to have suffered <b>Soul Damage</b>.";
          }
        }
        ChatMessage.create({
          speaker: { alias: token == null ? this.actor.name : token.name },
          content: `Has suffered a <b>${dragItem.woundtitle}</b> critical strike${effectMsg}`,
        });
        break;
      }
      case "damage": {
        // apply damage so it will update status and bar
        await this.actor.modifyTokenAttribute("tracked.hp", -dragItem.hits, true);
        ChatMessage.create({
          speaker: { alias: token == null ? this.actor.name : token.name },
          content: `Has taken ${dragItem.hits} damage.`,
        });
        break;
      }
      case "healing": {
        await this.actor.modifyTokenAttribute("tracked.hp", dragItem.hits, true);
        ChatMessage.create({
          speaker: { alias: token == null ? this.actor.name : token.name },
          content: `Has regained ${dragItem.hits} health.`,
        });
        break;
      }
      case "souldamage": {
        // apply damage so it will update status and bar
        const item = await this.actor.getNamedItem("dynamic.sd");
        await item.update({ "system.attr": Number(item.system.attr) + Number(dragItem.hits) });
        ChatMessage.create({
          speaker: { alias: token == null ? this.actor.name : token.name },
          content: `Has taken ${dragItem.hits} soul damage.`,
        });
        break;
      }
      case "db": {
        // apply the enemy defensive bonus so it will update status and bar
        const item = await this.actor.getNamedItem("dynamic.edb");
        await item.update({ "system.attr": Number(dragItem.db) });
        ChatMessage.create({
          speaker: { alias: token == null ? this.actor.name : token.name },
          content: `EDB has been set to ${dragItem.db}.`,
        });
        break;
      }
      case "severity": {
        // set the value of the critical severity item
        const item = await this.actor.getNamedItem("dynamic.critical_severity");
        const parts = dragItem.crit.split(" ");
        if (parts.length == 1) {
          await item.update({
            "system.label": parts[0],
            "system.formula": "+0",
            "system.value": 0,
          });
        } else {
          await item.update({
            "system.label": parts[0],
            "system.formula": parts[1],
            "system.value": Number(parts[1]),
          });
        }
        ChatMessage.create({
          speaker: { alias: token == null ? this.actor.name : token.name },
          content: `Has set Critical Severity to ${dragItem.crit}.`,
        });
        break;
      }
    }
  }

  async setStatusEffect(statusId, token) {
    if (!token.document.hasStatusEffect(statusId)) {
      const effect = CONFIG.statusEffects.find(e => e.id === statusId);
      await token.document.toggleActiveEffect(effect);
    }
  }

  async _onDrop(event) {
    // Try to extract the data
    let dragData;
    try {
      dragData = JSON.parse(event.dataTransfer.getData('text/plain'));
    } catch (err) {
      return false;
    }
    const actor = this.actor;

    // a contained item or wound effect will not have a uuid
    const temp = dragData.uuid?.split(".") || false;

    let itemids = {};
    if (temp) {
      for (let i = 0; i < temp.length; i++) {
        itemids[temp[i]] = temp[++i];
      }
    }

    const sameActor = (actor.isToken) ? actor.uuid.includes(itemids.Token) : actor.id == itemids.Actor;

    // wait for the item to be copied to the actor
    let item = await super._onDrop(event);

    if (sameActor) return item;
    if (item) {
      // a proper item was dropped and identified so unpack it or render it
      return this.dropItem(item);
    } else {
      // a piece of non-item data was dropped
      this.dropData(dragData, null);
    }
  }

  async _onTokenDrop(dragItem, token) {
    // wait for the item to be copied to the actor
    if (dragItem.type == "Item") {
      const item = await this._onDropItem(null, dragItem);
      return this.dropItem(item);
    } else {
      this.dropData(dragItem, token);
    }
  }

  async _onInputChange(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onInputChange()", [this, event]);

    const target = event.currentTarget;
    // get the item id
    const itemId = target.closest(".item").dataset.itemId;
    // get the name of the changed element
    const dataname = target.dataset.name;
    // get the new value
    const value = target.type === "checkbox" ? target.checked : target.value;
    // Get the original item.
    const item = this.actor.items.get(itemId);

    if (dataname == "system.alwaysOn") {
      item.update({ "system.alwaysOn": value, "system.inEffect": true });
      return;
    }

    // redirect changes to pool values
    if (target.className.includes("pool")) {
      const isDelta = value.startsWith("+") || value.startsWith("-");
      return this.actor.modifyTokenAttribute(`tracked.${system.slugify(target.dataset.abbr)}`, value, isDelta, true);
    }
    // update the item with the new value for the element or the actor if it is gmod
    item
      ? await item.update({ [dataname]: value })
      : await this.actor.update({ [dataname]: value });
  }

  _onItemCreate(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    const scriptdata = this.actor.system.itemscript || "";
    if (!dataset.type) {
      this._createItems(scriptdata);
    } else {
      this._createItem(dataset);
    }
  }

  _onItemExport() {
    const items = foundry.utils.duplicate(this.actor.items.contents);
    var exported = [];
    for (const item of items) {
      if (item.type == "Container") continue;
      delete item._id;
      delete item.flags;
      delete item.sort;
      delete item.effects;
      exported.push(item);
    }
    exported = JSON.stringify(exported);
    this.actor.update({ "system.itemscript": exported });
  }

  async _createItems(scriptdata) {
    // preserve newlines, etc - use valid JSON
    scriptdata = scriptdata
      .replace(/\\n/g, "\\n")
      .replace(/\\'/g, "\\'")
      .replace(/\\"/g, '\\"')
      .replace(/\\&/g, "\\&")
      .replace(/\\r/g, "\\r")
      .replace(/\\t/g, "\\t")
      .replace(/\\b/g, "\\b")
      .replace(/\\f/g, "\\f");
    // remove non-printable and other non-valid JSON chars
    scriptdata = scriptdata.replace(/[\u0000-\u0019]+/g, "");
    await this.actor.createEmbeddedDocuments("Item", JSON.parse(scriptdata));
    await this.actor.update({ "system.itemscript": "" });
    ui.notifications.info("Creation of multiple items completed!");
  }

  async _createItem(dataset) {
    const source = {
      name: dataset.type,
      type: dataset.type,
      system: {},
    };
    if (dataset.category) source.system.category = dataset.category;
    if (dataset.type == "Modifier") {
      source.system.attack = dataset.mod1 == "attack" || dataset.mod2 == "attack";
      source.system.defence =
        dataset.mod1 == "defence" || dataset.mod2 == "defence";
      source.system.skill = dataset.mod1 == "skill" || dataset.mod2 == "skill";
      source.system.spell = dataset.mod1 == "spell" || dataset.mod2 == "spell";
      source.system.check = dataset.mod1 == "check" || dataset.mod2 == "check";
      source.system.reaction =
        dataset.mod1 == "reaction" || dataset.mod2 == "reaction";
      source.system.damage = dataset.mod1 == "damage" || dataset.mod2 == "damage";
      source.system.primary =
        dataset.mod1 == "primary" || dataset.mod2 == "primary";
    }
    return await this.actor.createEmbeddedDocuments("Item", [source], {
      renderSheet: true,
    });
  }

  /**
   * Fetches the formatted modifiers for a roll.
   */
  fetchRelevantModifiers(actor, dataset) {
    const tempMods = [];
    const actormods = actor.items.filter((i) => i.type == "Modifier");
    const type = dataset.type == "modlist" ? dataset.modtype : dataset.type;

    // preload gmod
    tempMods.push({
      modifier: Number(actor.system.gmod.value) || 0,
      description: "global modifier",
    });

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const mod of actormods) {
      const moddata = mod.system;

      if (moddata.inEffect) {
        for (const entry of moddata.entries) {
          // check to see if this entry applies to this type of roll
          switch (type) {
            case "rms":
            case "check":
              if (entry.category != "skill") continue;
              break;
            case "dodge":
            case "block":
            case "parry":
              if (entry.category != "defence") continue;
              break;
            default:
              if (entry.category != type) continue;
          }
          if (Number(entry.moddedformula) == 0) continue;

          // what is the target of this modifier?
          let hasRelevantTarget = entry.targets.trim();
          // does this modifier have a target matching the item being rolled?
          if (hasRelevantTarget != "") {
            hasRelevantTarget = false;
            // get the array of targets to which it applies
            const cases = entry.targets.split(",").map(word => word.trim());
            for (const target of cases) {
              // test the target against the beginning of dataset.name for a match
              if (dataset.name.startsWith(target)) {
                hasRelevantTarget = true;
                continue;
              }
            }
          } else {
            // a general modifier
            hasRelevantTarget = true;
          }

          if (hasRelevantTarget) {
            let clean = entry.moddedformula?.replace(/\(([-+][\d]+)\)/g, "$1");
            tempMods.push({
              modifier: (clean == undefined) ? entry.value : clean[0] == "+" || clean[0] == "-" ? clean : "+" + clean,
              description: moddata.tempName
            });
          }
        }
      }
    }

    const prepareModList = mods => mods.map(mod => ({ ...mod, modifier: mod.modifier })).filter(mod => mod.modifier !== 0);

    return prepareModList(tempMods);
  }
}
