import { system } from "../config.js";
import { MyDialog } from "../dialog.js";

/**
 * Extend the base Actor document by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class BaseActor extends Actor {
  /**
   * Fetch and sort all the items, even though they will be prepared internally after this stage.
   */
  prepareBaseData() {
    const actordata = this.system;
    if (CONFIG.system.testMode)
      console.debug("entering prepareBaseData()\n", [this, actordata]);

    /**
     * Reference data structure to mimic static systems
     */
    actordata.dynamic = {};
    actordata.tracked = {};
    actordata.modifiers = {};
    actordata.sheetdata = {};

    // set some basic values
    actordata.dynamic.woundsbleed = { system: { moddedvalue: 0 } };
    actordata.dynamic.scale = { system: { moddedvalue: game.scenes?.current?.grid.distance || 5 }, };
    actordata.dynamic.visionType = "normal";
  }

  /**
   * Augment the basic actor data with additional dynamic data.
   *
   * Changes made here to item seem to stick.
   */
  prepareDerivedData() {
    const actor = this;
    const actordata = this.system;
    if (CONFIG.system.testMode)
      console.debug("entering prepareDerivedData()\n", [this, actordata]);


    const rollables = [];
    const skills = [];
    const primods = [];
    const mods = [];
    const nonmods = [];
    const rankitems = actordata.sheetdata.rankitems = [];

    let nextLayer = [];
    let previousLayer = [];
    const actorranks = {};

    // write the items into the model
    for (const item of actor.items.contents) {
      const itemdata = item.system;
      let itemID = system.slugify(item.name);
      // collect non-modifier items with formula
      if (itemdata.formula) nonmods.push(item);

      switch (item.type) {
        case "Primary-Attribute": {
          itemID = system.slugify(itemdata.abbr);
          // only process primaries with abbreviations
          if (itemID) {
            actordata.dynamic[itemID] = item;
            nextLayer.push(itemID);
          }
          break;
        }
        case "Pool": {
          itemID = system.slugify(itemdata.abbr);
          if (itemID) {
            actordata.tracked[itemID] = item;
            if (itemdata.hasmax && itemdata.hasmin) {
              nextLayer.push(itemID);
            } else {
              nonmods.push(item);
            }
          }
          break;
        }
        case "Variable": {
          // add any items whose name starts with "Ranks:" to the list to be polled next
          if (item.name.startsWith("Ranks:")) rankitems.push(item);
        }
        case "Rollable": {
          actordata.dynamic[itemID] = item;
          if (Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#"))
            nextLayer.push(itemID);
          skills.push(item);
          break;
        }
        case "Ranged-Attack":
        case "Melee-Attack":
        case "Defence": {
          actordata.dynamic[itemID] = item;
          if (Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#"))
            nextLayer.push(itemID);
          rollables.push(item);
          break;
        }
        case "Trait": {
          switch (itemdata.category) {
            case "perk": {
              // Traits that define a vision type for the actor
              switch (item.name) {
                case "Keen Senses":
                case "Night Sight":
                case "Dark Sight":
                case "Darkvision":
                case "Star Sight": {
                  actordata.dynamic.visionType = item.name;
                }
              }
              break;
            }
          }
          break;
        }
        case "Modifier": {
          itemdata.tempName = item.name;
          actordata.modifiers[itemID] = item;
          mods.push(item);
          if (itemdata.primary) primods.push(item);
          break;
        }
      }
    }

    /**
     * Process Ranks Calculation
     */
    if (CONFIG.system.testMode) console.debug("Rankitems to be polled:\n", [this, rankitems]);

    // poll the rank items for systems which count them
    for (const item of rankitems) {
      for (const entry of item.system.entries) {
        const target = system.slugify(entry.label);
        // if the name is not blank
        if (target) {
          entry.value = Number(entry.formula);
          // the target may be one that does not exist on the actor so capture it anyway
          actorranks[target] = actorranks[target] ? actorranks[target] + entry.value : entry.value;
        }
      }
    }

    if (CONFIG.system.testMode) console.debug("Rankdata after polling:\n", [this, actorranks]);

    // assign the ranks to the targets in dynamic
    for (const [name, ranks] of Object.entries(actorranks)) {
      const item = actordata.dynamic[name];
      if (item) {
        const itemdata = item.system;
        // calculate rank bonus to assign to value and moddedvalue
        if (item.type == "Primary-Attribute") {
          itemdata.attr = ranks;
        } else {
          // set the formula text field
          itemdata.formula = `#${ranks}`;
        }
        itemdata.moddedvalue = itemdata.value = itemdata.ranks = ranks;
      } else {
        if (CONFIG.system.testMode)
          console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
      }
    }

    /**
     * Process Primary-Attribute Modifiers
     */
    for (const item of primods) {
      const itemdata = item.system;
      if (itemdata.inEffect) {
        for (const entry of itemdata.entries) {
          if (entry.category == "primary") {
            // process formulae before applying the mod
            actor.processModifierFormula(entry, itemdata);
            const cases = entry.targets.split(",").map(word => word.trim().toLowerCase());
            for (const key of cases) {
              const target = actordata.dynamic[key];
              if (target?.type == "Primary-Attribute") {
                target.system.value = target.system.moddedvalue += entry.value;
              }
            }
          }
        }
      }
    }

    // this will store all valid modifier entries for later processing efficiency
    const activeModEntries = [];

    /**
     * Process all the modifiers with numeric values.
     */
    for (const item of mods) {
      const itemdata = item.system;
      if (!itemdata.inEffect) continue;
      for (const entry of itemdata.entries) {
        if (Number.isNumeric(entry.formula)) {
          entry.value = Number(entry.formula);
          activeModEntries.push(entry);
        }
      }
    }

    /**
     * Fetch the modifiers for each skill and calculate the moddedvalue.
     */
    for (const item of skills) {
      const itemdata = item.system;
      itemdata.moddedvalue = itemdata.value;
      // if there is no modified formula, make it the value
      if (itemdata.moddedformula == undefined)
        itemdata.moddedformula = "" + itemdata.value;
      itemdata.moddedvalue += this.fetchDisplayModifiers(item.name, "skill", activeModEntries);
    }

    /**
     * Calculate every item with a formula, one layer at a time, starting with
     * those whose references have been calculated. Once an item has been
     * processed, add it to the layer of those available as references.
     */
    while (nextLayer.length !== 0) {
      previousLayer = previousLayer.concat(nextLayer);
      nextLayer = [];

      for (const item of nonmods) {
        const itemdata = item.system;
        if (item.type == "Pool") {
          // new pool formula functionality

          const dynID = system.slugify(itemdata.abbr);

          //ignore numeric formulae
          if (itemdata.hasmax && itemdata.hasmin) continue;

          if (!itemdata.hasmax) {
            // formula has not yet been calculated
            for (const target of previousLayer) {
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.maxForm.includes(target)) {
                // write to Actor Item
                let formData = this._replaceData(itemdata.maxForm);
                try {
                  itemdata.max = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")) * 100) / 100;
                  itemdata.moddedvalue = itemdata.value = Math.min(itemdata.value, itemdata.max);
                } catch (err) {
                  itemdata.max = 0; // eliminate any old values
                  console.warn("Pool-max formula evaluation error:\n", [actor.name, item.name, itemdata.maxForm,]);
                }
                itemdata.hasmax = true;
              }
            }
          }
          if (!itemdata.hasmin) {
            // formula has not yet been calculated
            for (const target of previousLayer) {
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.minForm.includes(target)) {
                // write to Actor Item
                let formData = this._replaceData(itemdata.minForm);
                try {
                  itemdata.min = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")) * 100) / 100;
                  itemdata.moddedvalue = itemdata.value = Math.max(itemdata.value, itemdata.min);
                } catch (err) {
                  itemdata.min = 0; // eliminate any old values
                  console.warn("Pool-min formula evaluation error:\n", [actor.name, item.name, itemdata.minForm,]);
                }
                itemdata.hasmin = true;
              }
            }
          }
          itemdata.ratio = Math.trunc(itemdata.value / itemdata.max * 100);
          if (itemdata.hasmax && itemdata.hasmin)
            nextLayer.push(dynID);
        } else {
          //ignore items with no formula or whose formula has already been processed
          if (!itemdata.formula || Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#") || (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != ""))
            continue;
          const dynID = system.slugify(item.name);
          if (previousLayer.includes(dynID)) continue;

          // the formula is a reference
          if (itemdata.formula.includes("@")) {
            const regex = /@([\w]+)/g;
            let foundAllRefs = true;
            let match;
            while ((match = regex.exec(itemdata.formula)) && foundAllRefs) {
              foundAllRefs = previousLayer.includes(match[1]);
            }
            if (foundAllRefs) {
              let formData = this._replaceData(itemdata.formula);
              try {
                itemdata.moddedvalue = itemdata.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")) * 100) / 100;
                itemdata.moddedformula = "" + itemdata.value; // store the result as a String
              } catch (err) { // TODO: this may not be reachable now that we are checking all the refs first
                // store the formula ready to be rolled
                itemdata.moddedformula = formData.value;
                itemdata.moddedvalue = itemdata.value = 0; // eliminate any old values
                console.warn("Non-Modifier formula evaluation incomplete:\n", [actor.name, item.name, itemdata.moddedformula]);
              }
              // put this item into the next layer and stop looking for matches
              nextLayer.push(dynID);
            }
          } else {
            // the formula might be a dice expression
            if (itemdata.formula.toLowerCase().includes("d")) {
              itemdata.moddedformula = itemdata.formula;
              itemdata.moddedvalue = itemdata.value = 0; // eliminate any old values
            }
          }
        }
      }
    }

    /**
     * Process all the modifiers with formulae.
     */
    for (const item of mods) {
      const itemdata = item.system;
      if (!itemdata.inEffect) continue;
      for (const entry of itemdata.entries) {
        if (Number.isNumeric(entry.formula)) continue;
        if (entry.formula == "") continue;
        // the modifier needs to have a formula to get to here.
        let formData = this._replaceData(entry.formula);
        try {
          entry.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")));
          entry.moddedformula = "" + entry.value; // store the result as a String
        } catch (err) {
          // store the formula ready to be rolled
          entry.moddedformula = formData.value;
          entry.value = 0; // eliminate any old values
          console.warn("Modifier formula evaluation error:\n", [this.name, item.name, entry.moddedformula,]);
        }
        if (!itemdata.tempName.includes(formData.label))
          itemdata.tempName += formData.label;
        activeModEntries.push(entry);
      }
    }

    /**
     * Fetch the modifiers for each rollable item and calculate the
     * moddedvalue.
     */
    for (const item of rollables) {
      const itemdata = item.system;
      itemdata.moddedvalue = itemdata.value;
      // if there is no modified formula, make it the value
      if (itemdata.moddedformula == undefined)
        itemdata.moddedformula = "" + itemdata.value;
      switch (item.type) {
        case "Melee-Attack":
        case "Ranged-Attack": {
          itemdata.moddedvalue += this.fetchDisplayModifiers(item.name, "attack", activeModEntries);
          break;
        }
        case "Defence": {
          itemdata.moddedvalue += this.fetchDisplayModifiers(item.name, "defence", activeModEntries);
          break;
        }
      }
    }

  }

  processModifierFormula(entry, itemdata) {
    if (!Number.isNumeric(entry.formula) && entry.formula != "") {
      const formData = this._replaceData(entry.formula);
      try {
        entry.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")));
        entry.moddedformula = "" + entry.value; // store the result as a String
      } catch (err) {
        // store the formula ready to be rolled
        entry.moddedformula = formData.value;
        entry.value = 0; // eliminate any old values
        console.warn("Modifier formula evaluation error:\n", [this.name, itemdata.tempName, entry.moddedformula,]);
      }
      if (!itemdata.tempName.includes(formData.label)) itemdata.tempName += formData.label;
    }
  }

  getNamedItem(name) {
    let parts = name.split(".");
    if (parts.length != 2) return undefined;
    return this.system[parts[0]][parts[1]];
  }


  /**
   * Replace data references in the formula of the syntax `@attr[.fieldname =.moddedvalue]` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    const dataRgx = /[@]([\w.]+)/gi;
    const dynamic = this.system.dynamic;
    const tracked = this.system.tracked;
    let tempName = "";
    const findTerms = (match, term) => {
      const fields = term.split(".");
      const attribute = fields[0];
      // default to the field "moddedvalue" if none is specified
      const field = fields[1] || "moddedvalue";
      if (dynamic[attribute]) {
        const value = dynamic[attribute].system[field];
        const label = dynamic[attribute].system.label;
        tempName += (label) ? ` (${label})` : "";
        return (value) ? String(value).trim() : "0";
      } else if (tracked[attribute]) {
        const value = tracked[attribute].system[field];
        return (value) ? String(value).trim() : "0";
      } else {
        return "0";
      }
    };
    const replyData = formula.replace(dataRgx, findTerms).replace(/(min)|(max)|(floor)|(ceil)|(round)|(abs)|(pow)/g, "Math.$&");
    return { value: replyData, label: tempName };
  }

  async setConditions(newValue, attrName) {
    if (CONFIG.system.testMode)
      console.debug("entering setConditions()\n", [newValue, attrName]);

    const attr = attrName.split(".")[2];
    const item = this.system.tracked[attr];
    const itemdata = item.system;
    let attrValue = itemdata.value;
    let attrMax = itemdata.max;
    let attrState = itemdata.state;
    let attrMin = itemdata.min;

    // Assign the variables
    if (attrName.includes(".max")) {
      attrMax = Math.round(eval(this._replaceData(newValue).value.replace(system.dataRgx, "")));
    } else if (attrName.includes(".min")) {
      attrMin = Math.round(eval(this._replaceData(newValue).value.replace(system.dataRgx, "")));
    } else {
      attrValue = newValue;
    }
    const ratio = attrValue / attrMax;

    switch (attr) {
      case "hp": {
        // use eighths and let the user set the minimum
        switch (Math.trunc(ratio * 8)) {
          case 1: {
            attrState = "[1/8]";
            break;
          }
          case 2: {
            attrState = "[1/4]";
            break;
          }
          case 3: {
            attrState = "[3/8]";
            break;
          }
          case 4: {
            attrState = "[1/2]";
            break;
          }
          case 5: {
            attrState = "[5/8]";
            break;
          }
          case 6: {
            attrState = "[3/4]";
            break;
          }
          case 7: {
            attrState = "[7/8]";
            break;
          }
          case 8: {
            attrState = "[FIT]";
            break;
          }
          default: {
            // bad shape
            if (attrValue <= attrMin) {
              attrState = "[DEAD]";
            } else if (attrValue <= 0) {
              attrState = "[UNC]";
            } else {
              attrState = "[< 1/8]";
            }
          }
        }
        break;
      }
      default: {
        // assume that the state is measured in eighths
        switch (Math.trunc(ratio * 8)) {
          case 1: {
            attrState = "[1/8]";
            break;
          }
          case 2: {
            attrState = "[1/4]";
            break;
          }
          case 3: {
            attrState = "[3/8]";
            break;
          }
          case 4: {
            attrState = "[1/2]";
            break;
          }
          case 5: {
            attrState = "[5/8]";
            break;
          }
          case 6: {
            attrState = "[3/4]";
            break;
          }
          case 7: {
            attrState = "[7/8]";
            break;
          }
          case 8: {
            attrState = "[Full]";
            break;
          }
          default: {
            // dead
            if (ratio <= 0) {
              // empty
              attrState = "[Empty]";
            } else {
              attrState = "[< 1/8]";
            }
          }
        }
      }
    }
    itemdata.min = attrMin;
    itemdata.value = attrValue;
    itemdata.max = attrMax;
    itemdata.state = attrState;

    return await item.update({ system: itemdata });
  }

  /**
   * Handle how changes to a Token attribute bar are applied to the Actor.
   * Logic for pools to be edited by plus-minus and text input (on Actor and Item) is also redirected here.
   * @param {string} attribute    The attribute path
   * @param {number} value        The target attribute value
   * @param {boolean} isDelta     Whether the number represents a relative change (true) or an absolute change (false)
   * @param {boolean} isBar       Whether the new value is part of an attribute bar, or just a direct value
   * @return {Promise}
   */
  async modifyTokenAttribute(attribute, value, isDelta = false, isBar = true) {
    if (CONFIG.system.testMode) console.debug("entering modifyTokenAttribute()\n", [attribute, value, isDelta, isBar,]);

    if (isBar && !attribute.endsWith(".system"))
      attribute += ".system";
    // fetch the pool if the attribute changing is 'value', or the attribute itelf if not
    const current = foundry.utils.getProperty(this.system, attribute);
    // isBar is true for the value and must be clamped
    // isBar is false for the min or max
    value = isBar
      ? Math.clamp(current.min, isDelta ? Number(current.value) + Number(value) : Number(value), current.max)
      : isDelta ? Number(current) + Number(value) : value;

    // redirect updates to setConditions
    return await this.setConditions(value, `system.${attribute}`);
  }

  /**
   * Fetches the total modifier value for a rollable item
   */
  fetchDisplayModifiers(name, type, activeModEntries) {
    let mods = 0;
    const modEntries = activeModEntries.filter(function (ame) {
      if (ame.category == type) return true;
    });
    for (const entry of modEntries) {
      // what is the target of this modifier?
      let hasRelevantTarget = entry.targets.trim();
      // does this modifier have a target matching the item being rolled?
      if (hasRelevantTarget != "") {
        hasRelevantTarget = false;
        // get the array of targets to which it applies
        const cases = entry.targets.split(",").map((word) => word.trim());
        for (const target of cases) {
          // test the target against the beginning of dataset.name for a match
          if (name.startsWith(target)) {
            hasRelevantTarget = true;
            continue;
          }
        }
      } else {
        // a general modifier
        hasRelevantTarget = true;
      }
      mods += hasRelevantTarget ? entry.value : 0;
    }
    return mods;
  }

  /**
   * Resets all temporary Variables and Modifiers
   */
  resetModVars(temporary = true) {
    const items = this.items.filter(function (item) {
      if (temporary) {
        return item.system.temporary;
      } else {
        return item.system.once;
      }
    });
    let updates = [];

    for (const item of items) {
      switch (item.type) {
        case "Modifier": {
          updates.push({ _id: item.id, ["system.inEffect"]: false, });
          break;
        }
        case "Variable": {
          updates.push({
            _id: item.id,
            ["system.value"]: item.system.entries[0].value,
            ["system.formula"]: item.system.entries[0].formula,
            ["system.label"]: item.system.entries[0].label,
          });
          break;
        }
      }
    }
    this.updateEmbeddedDocuments("Item", updates);
  }

  /**
   * Apply the amount of bleeding as damage and send a message to the chat.
   */
  async applyBleeding() {
    const bleed = this.system.dynamic.woundsbleed.system.moddedvalue;
    if (bleed && bleed != 0) {
      // apply damage so it will update status and bar
      await this.modifyTokenAttribute("tracked.hp", -bleed, true);
      ChatMessage.create({
        speaker: { actor: this.id },
        content: `Has taken ${bleed} bleeding damage.`,
      });
    } else return;
  }

  /**
   * A method to dispatch non-event-driven rolls.
   */
  async roll(rolldata) {
    //=============================================
    let flavour = rolldata.flavour || "";
    let threat = rolldata.threat || 20;

    let fail = 1;
    let canCrit = false,
      isModList = false;

    let withAdvantage = rolldata.actor.system.dynamic.withadvantage?.system.formula || false;
    const advantageVariable = await rolldata.actor.getNamedItem("dynamic.withadvantage");

    let rolledInitiative = rolldata.name == "Initiative";
    let hideTotal = false;

    switch (rolldata.type) {
      case "tochat":
      case "technique": { // send the notes to the chat and return
        const isplaintext = rolldata.roll.replace(/<[^>]*>?/gm, '') == rolldata.roll;
        ChatMessage.create({
          speaker: ChatMessage.getSpeaker({ alias: this.name }),
          flavor: `<b>${rolldata.name}</b><hr>`,
          content: isplaintext ? rolldata.roll.replace(/(?:\r\n|\r|\n)/g, '<br>') : rolldata.roll,
        });
      }
      case "rms": {
        // no roll, just storing a value
        return;
      }
    }

    // get the modifiers
    let modList = rolldata.actor.sheet.fetchRelevantModifiers(
      rolldata.actor,
      { type: rolldata.type, modtype: rolldata.modtype, name: rolldata.name }
    );
    // process the modifiers
    let modformula = "";
    flavour +=
      rolldata.type != "modlist"
        ? rolldata.type == "damage"
          ? ` [<b>${rolldata.roll} ${rolldata.damageType}</b>]`
          : ` [<b>${rolldata.roll}</b>]`
        : `<p class="chatmod">[<b>${rolldata.roll}</b>]: ${rolldata.name}<br>`;
    let hasMods = modList.length != 0;
    if (hasMods) {
      var sign;
      flavour += rolldata.type != "modlist" ? `<p class="chatmod">` : "";
      for (const mod of modList) {
        sign =
          typeof mod.modifier === "string" ? "" : mod.modifier > -1 ? "+" : "";
        modformula += `${sign}${mod.modifier}`;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    var formula = "";
    switch (rolldata.type) {
      case "modlist": // this will render a dialog with the modifiers, not a chat message
        isModList = true;
      case "block": // send the value to the chat as a modifiable roll
      case "damage": {
        // roll damage using a valid dice expression
        formula = rolldata.roll;
        break;
      }
      case "check": {
        // a valid dice expression
        formula = rolldata.roll;
        break;
      }
      case "parry":
      case "spell":
      case "attack": // attacks are subject to critical results
      case "dodge": // a d20 roll to exceed a target number
      case "skill": {
        // normal d20 attacks
        formula = withAdvantage ? `${withAdvantage}oe + ` + rolldata.roll : "1d20oe + " + rolldata.roll;
        canCrit = true;
        break;
      }
      default: {
        // there should not be any of these
        console.error("Failed to process rolldata roll type: " + rolldata.type);
      }
    }

    // process the modified roll
    let r = await new Roll(formula + modformula).evaluate();

    if (isModList) {
      // render the modlist dialog and return
      flavour += `<hr><p>${r.result} = <b>${r.total}</b></p>`;
      new MyDialog({
        title: rolldata.actor.name,
        content: flavour,
        buttons: {
          close: {
            icon: "<i class='fas fa-check'></i>",
            label: "Close",
          },
        },
        default: "close",
      }).render(true);
      return;
    }

    if (canCrit) {
      // an attack or defence roll in D120
      let critfail = false;
      let critsuccess = false;
      if (r.terms[0].total <= fail) {
        critfail = true;
      } else if (r.terms[0].total >= threat) {
        critsuccess = true;
      }
      flavour += ` <p>D20 Roll: [<b>${r.terms[0].total}</b>]`;
      if (critsuccess)
        flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Nat 20</span>`;
      else if (critfail)
        if (r.terms[0].results[1]?.result == -1) {
          flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
        } else {
          flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Nat 1</span>`;
        }
      flavour += `</p>`;
    }
    if (advantageVariable) {
      let state = advantageVariable.system.label;
      switch (state) {
        case "Advantage":
          flavour += ` <p class="critsuccess">With ${state}</p>`;
          break;
        case "Disadvantage":
          flavour += ` <p class="critfail">With ${state}</p>`;
          break;
      }
    }

    switch (rolldata.type) {
      case "damage": {
        const content = await renderTemplate("systems/veil-rider/templates/chat/content-damage.hbs", {
          actor: this,
          modlist: modList,
          total: r.total,
          rolldata: rolldata
        });

        // prepare the flavor in advance based on which type of die is in use.
        r.toMessage(
          {
            speaker: ChatMessage.getSpeaker({ alias: this.name }),
            flavor: content,
            flags: { hideMessageContent: false }
          }
        );
        break;
      }
      default: {
        r.toMessage({
          speaker: ChatMessage.getSpeaker({ alias: this.name }),
          flavor: flavour,
          flags: { hideMessageContent: hideTotal },
        });
      }
    }

    let updates = { ["system.gmod.value"]: 0 };
    if (rolledInitiative) {
      updates["system.bs.value"] = r.total;
    }
    rolldata.actor.resetModVars(false);
    rolldata.actor.update(updates);
    return;
  }

  showJournal(journalPage) {
    system.showJournal(journalPage);
  }

  /**
   * Pass a filtering object to this method in the form:
   * {
   *  type: "",
   *  category: "",
   *  group: "",
   *  target: "",
   *  position: {
   *   width: 300,
   *   top: 0,
   *   left: 0
   *  }
   * }
   * where the values shown are the defaults and may be omitted
   * if not required.
   *
   * Each field is a comma-separated list of the things you want
   * to include in the result. It matches against the text you
   * enter, so a category of "s" will match skill, spell and rms.
   *
   * type: may be any of {Primary, Melee, Ranged,
   * Attack, Rollable, Defence}
   *
   * category: applies only to type:
   *     Rollable: {check, skill, spell, technique, rms} and
   *     Defence: {dodge, parry, block}
   * and may be used without specifying the type.
   *
   * group: is a user-defined field for use in filtering
   * this result.
   *
   * target: Any case-sensitive portion of the item name.
   */
  rollables(macroData) {
    macroData.position = macroData.position || { width: 300, top: 0, left: 0 };
    macroData.type = macroData.type || "";
    macroData.category = macroData.category || "";
    macroData.group = macroData.group || "";
    macroData.target = macroData.target || "";
    macroData.flavour = macroData.flavour || "";

    const rollables = this.items
      .filter(function (item) {
        if ("ContainerTraitPoolModifierVariableAbilityEquipment".includes(item.type)) return null;
        if (macroData.target != "") {
          const cases = macroData.target.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.name.includes(target))) return null;
        }
        if (macroData.group != "") {
          const cases = macroData.group.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.system.group.includes(target))) return null;
        }
        if (macroData.category != "") {
          const cases = macroData.category.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.system.category?.includes(target))) return null;
        } else {
          const cases = macroData.type.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.type.includes(target))) return null;
        }
        return item;
      })
      .sort((a, b) => {
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
        return 0;
      });

    switch (rollables.length) {
      case 0:
        ui.notifications.error("Your rollmacro call produced no results");
        break;
      case 1: // go straight to the roll dialog with modifiers
        this.modifierdialog(rollables[0]._id, macroData);
        break;
      default: // provide the choice of results
        this.rollabledialog(rollables, macroData);
        break;
    }
  }

  async toggleStatusFromFolderItem(itemdata) {
    // the status has already been toggled
    const modref = system.slugify(itemdata.name);
    let existing = this.system.modifiers?.[modref] || null;
    if (existing == null) {
      await this.createEmbeddedDocuments("Item", [itemdata], { renderSheet: false, });
      existing = this.system.modifiers?.[modref];
    }
    const toggledOn = this.statuses.has(modref);
    if (toggledOn) { // if necessary, update/add the modifier
      if (!CONFIG.system.entriesAreEqual(itemdata.system.entries, existing.system.entries)) {
        await this.deleteEmbeddedDocuments("Item", [existing._id]);
        await this.createEmbeddedDocuments("Item", [itemdata], { renderSheet: false, });
        existing = this.system.modifiers?.[modref];
      }
    }
    existing.update({ system: { inEffect: toggledOn, alwaysOn: false } });
  }


  async rollabledialog(rollabledata, macroData) {
    const actor = this;
    const template = "systems/veil-rider/templates/rollmacro/rollables.hbs";
    const html = await renderTemplate(template, { rollabledata });

    new MyDialog(
      {
        title: `Rollables: ${actor.name}`,
        content: html,
        buttons: {},
        options: {
          actor: actor,
          macroData: macroData,
        },
      },
      {
        width: macroData.position.width || 300,
        top: macroData.position.top || 0,
        left: macroData.position.left || 0,
      }
    ).render(true);
  }

  // a test method for executing token actions. SysDev part 8 covers a better way to do this.
  async modifierdialog(rollableId, macroData) {
    const rollabledata = this.items.get(rollableId);
    const actor = this;
    const name = rollabledata.name;
    const type = rollabledata.type;
    const category = rollabledata.system.category || null;

    const modifiers = [];
    for (const mod of Object.values(this.system.modifiers)) {
      for (let entry of Object.values(mod.system.entries)) {
        if (entry.formula == "") continue;
        // check to see if this entry applies to this type of roll
        switch (type) {
          case "Melee-Attack":
          case "Ranged-Attack": {
            if (!["damage", "attack"].includes(entry.category)) continue;
            break;
          }
          case "Defence":
          case "Rollable": {
            if (category != entry.category) continue;
            break;
          }
          case "Primary-Attribute": {
            if (!entry.targets.toLowerCase().includes(rollabledata.system.abbr.toLowerCase())) continue;
            break;
          }
        }

        let hasRelevantTarget = entry.targets.trim();
        // does this modifier have targets?
        if (hasRelevantTarget != "") {
          hasRelevantTarget = false;
          // get the array of targets to which it applies
          const cases = entry.targets.split(",").map((word) => word.trim());
          for (const target of cases) {
            // test the target against the beginning of name for a match
            if (name.startsWith(target)) {
              hasRelevantTarget = true;
              continue;
            }
          }
        } else {
          // a general modifier
          hasRelevantTarget = true;
        }
        if (hasRelevantTarget) {
          // is the modifier already in the list?
          if (modifiers.includes(mod)) continue;
          // does the modifier have a reference?
          if (entry.formula.includes("@")) {
            // is the reference to a Variable or Pool?
            const regex = /@([\w.\-]+)/gi;
            const reference = regex.exec(entry.formula)[1];
            const varpool = actor.system.tracked[reference] || actor.system.dynamic[reference];
            if (varpool) {
              if (!modifiers.includes(varpool)) modifiers.push(varpool);
            }
          }
          modifiers.push(mod);
        }
      }
    }
    // add withAdvantage to every dialog
    const varpool = actor.system.dynamic.withadvantage;
    if (varpool) {
      if (!modifiers.includes(varpool)) modifiers.push(varpool);
    }
    console.log([name, type, category]);
    const uniquemods = [...new Set(modifiers)];

    // Is there at least one target selected so we can fetch it's defence data?
    const hasTarget = game.user.targets.size > 0 && (type == "Melee-Attack" || type == "Ranged-Attack");
    const target = game.user.targets.first()?.name || "No Target";

    const template = "systems/veil-rider/templates/rollmacro/modifiers.hbs";
    const html = await renderTemplate(template, { // can add target data here too (see VsD)
      actor,
      rollabledata,
      modifiers,
      hasTarget,
      target
    });

    new MyDialog(
      {
        title: `Rollable: ${actor.name}`,
        content: html,
        buttons: {
          back: {
            label: "Back",
            callback: (html) => {
              this.rollables(macroData);
            },
          },
        },
        options: {
          actor: actor,
          macroData: macroData,
        },
      },
      {
        width: macroData.position.width || 300,
        top: macroData.position.top || 0,
        left: macroData.position.left || 0,
      }
    ).render(true);
  }
}
