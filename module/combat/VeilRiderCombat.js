import { system } from "../config.js";

export class VeilRiderCombat extends Combat {

    async startCombat() {
        await this.setupTurns();
        return super.startCombat();
    }

    /**
     * @override
     */
    async rollAll(options) {
        await super.rollAll(options);
        // make sure the top combatant is selected
        return this.update({ turn: 0 });
    }

    async nextRound() {
        await this.fetchActorData();
        return super.nextRound();
    }

    async fetchActorData() {
        this.combatants.forEach(c => c.actor.resetModVars(true));
        for (let t of this.turns) {
            t.actions = t.getFlag("veil-rider", "actions") + t.actor.system.dynamic.apr?.system.moddedvalue;
            let actionbase = t.actor.system.dynamic.apbase?.system.moddedvalue || 15;
            await t.setFlag("veil-rider", "actions", Math.min(actionbase, t.actions));
        }
    }
}

export class VeilRiderCombatTracker extends CombatTracker {
    get template() {
        return "systems/veil-rider/templates/combat/vrcombat-tracker.hbs";
    }

    _onConfigureCombatant(li) {
        const combatant = this.viewed.combatants.get(li.data('combatant-id'));
        new VeilRiderCombatantConfig(combatant, {
            top: Math.min(li[0].offsetTop, window.innerHeight - 350),
            left: window.innerWidth - 720,
            width: 400
        }).render(true);
    }

    async getData(options) {
        const data = await super.getData(options);

        if (!data.hasCombat) {
            return data;
        }

        for (let turn of data.turns) {
            let combatant = this.viewed.combatants.get(turn.id);
            turn.actions = combatant.getFlag("veil-rider", "actions");
        }
        return data;
    }

    activateListeners(html) {
        super.activateListeners(html);
        html.find(".actions").change(this._onActionsChanged.bind(this));
        html.find(".spend").click(this._onSpend.bind(this));
        html.find(".regain").click(this._onRegain.bind(this));
        html.find(".initiative").change(this._onInitiativeChanged.bind(this));
    }

    async _onActionsChanged(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const c = this.viewed.combatants.get(li.dataset.combatantId);
        if (!c.isOwner) return;
        await c.update({ ["flags.veil-rider.actions"]: btn.value });
    }

    async _onInitiativeChanged(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const c = this.viewed.combatants.get(li.dataset.combatantId);
        if (!c.isOwner) return;
        await c.update({ initiative: btn.value });
    }

    async _onSpend(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const c = this.viewed.combatants.get(li.dataset.combatantId);
        if (!c.isOwner) return;
        await c.update({ ["flags.veil-rider.actions"]: c.getFlag("veil-rider", "actions") - 1 });
    }

    async _onRegain(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const c = this.viewed.combatants.get(li.dataset.combatantId);
        if (!c.isOwner) return;
        await c.update({ ["flags.veil-rider.actions"]: c.getFlag("veil-rider", "actions") + 1 });
    }
}

export class VeilRiderCombatantConfig extends CombatantConfig {
    get template() {
        return "systems/veil-rider/templates/combat/vrcombatant-config.hbs";
    }
}

export class VeilRiderCombatant extends Combatant {
    _onCreate(data, options, userId) {
        super._onCreate(data, options, userId);
        if (this.isOwner)
            this.setFlag("veil-rider", "actions", this.actor.system.dynamic.apbase?.system.moddedvalue);
    }
}
