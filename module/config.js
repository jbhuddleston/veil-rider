export const system = {
  dataRgx: /[^-(){}\d<>/=*+., DdflorceiMmaxthbsunvpwkxX]/g,
  slugify: (text) => {
    return text
      .toString() // Cast to string
      .toLowerCase() // Convert the string to lowercase letters
      .normalize("NFD") // The normalize() method returns the Unicode Normalization Form of a given string.
      .trim() // Remove whitespace from both sides of a string
      .replace(/[\s-]+/g, "_") // Replace spaces with underscore
      .replace(/[^\w]+/g, "") // Remove all non-word chars
      .replace(/\_\_+/g, "_"); // Replace multiple underscores with a single one
  },
  entriesAreEqual: (first, second) => {
    if (first.length != second.length) return false;
    for (let i = 0; i < first.length; i++) {
      if (first[i].formula != second[i].formula) return false;
      if (first[i].category != second[i].category) return false;
      if (first[i].targets != second[i].targets) return false;
    }
    return true;
  },
  showJournal: (journalName) => {
    const journalref = system.journalref[system.slugify(journalName)];
    if (journalref) {
      Journal._showEntry(journalref.uuid);
    } else {
      if (journalName)
        ui.notifications.error(
          `There is no matching Journal Entry for ${journalName} in the Compendia.`
        );
    }
  },
  closeJournal: (journalName) => {
    const journalref = system.journalref[system.slugify(journalName)];
    if (journalref.parent) {
      journalref.parent.sheet.close();
    } else {
      journalref.sheet.close();
    }
  },
  combat: {
    defaultActionCost: 1,
    defaultShotCost: 3,
    missingInitiative: "You must roll initiative first."
  },
  selectOptions: {
    defence_category: [
      {value: "block", label: "local.item.Defence.block"}
    ],
    modifier_category: [
      {value: "attack", label: "local.item.Modifier.attack"},
      {value: "damage", label: "local.item.Modifier.damage"},
      {value: "defence", label: "local.item.Modifier.defence"},
      {value: "skill", label: "local.item.Modifier.skill"},
      {value: "primary", label: "local.item.Modifier.primary"}
      ],
    rollable_category: [
      {value: "check", label: "local.item.Rollable.check"},
      {value: "skill", label: "local.item.Rollable.skill"},
      {value: "rms", label: "local.item.Rollable.rms"}
      ],
    trait_category: [
      {value: "advantage", label: "local.item.Trait.advantage"},
      {value: "disadvantage", label: "local.item.Trait.disadvantage"},
      {value: "perk", label: "local.item.Trait.perk"},
      {value: "quirk", label: "local.item.Trait.quirk"}
      ]
  },
  variables: {
    skillvariables: ["withadvantage"],
    spellvariables: [],
    defencevariables: [],
    attackvariables: ["withadvantage", "range", "ammo"],
  },
  postures: {
    standing: "local.postures.standing",
    crouching: "local.postures.crouching",
    kneeling: "local.postures.kneeling",
    crawling: "local.postures.crawling",
    sitting: "local.postures.sitting",
    pronef: "local.postures.pronef",
    proneb: "local.postures.proneb"
  }
};

CONFIG.ChatMessage.template = "systems/veil-rider/templates/chat/chat-message.hbs";

CONFIG.postures = [
  "systems/veil-rider/icons/postures/standing.png",
  "systems/veil-rider/icons/postures/sitting.png",
  "systems/veil-rider/icons/postures/crouching.png",
  "systems/veil-rider/icons/postures/crawling.png",
  "systems/veil-rider/icons/postures/kneeling.png",
  "systems/veil-rider/icons/postures/lyingback.png",
  "systems/veil-rider/icons/postures/lyingprone.png",
  "systems/veil-rider/icons/postures/sittingchair.png"
];

CONFIG.sizemods = [
  "systems/veil-rider/icons/sizemods/smneg1.png",
  "systems/veil-rider/icons/sizemods/smneg2.png",
  "systems/veil-rider/icons/sizemods/smneg3.png",
  "systems/veil-rider/icons/sizemods/smneg4.png",
  "systems/veil-rider/icons/sizemods/smpos1.png",
  "systems/veil-rider/icons/sizemods/smpos2.png",
  "systems/veil-rider/icons/sizemods/smpos3.png",
  "systems/veil-rider/icons/sizemods/smpos4.png"
];

CONFIG.crippled = [
  "systems/veil-rider/icons/crippled/crippledleftarm.png",
  "systems/veil-rider/icons/crippled/crippledlefthand.png",
  "systems/veil-rider/icons/crippled/crippledleftleg.png",
  "systems/veil-rider/icons/crippled/crippledleftfoot.png",
  "systems/veil-rider/icons/crippled/crippledrightarm.png",
  "systems/veil-rider/icons/crippled/crippledrighthand.png",
  "systems/veil-rider/icons/crippled/crippledrightleg.png",
  "systems/veil-rider/icons/crippled/crippledrightfoot.png",
];

CONFIG.statusEffectsVR = [
  { img: 'systems/veil-rider/icons/postures/standing.png', id: 'standing', name: 'local.postures.standing' },
  { img: 'systems/veil-rider/icons/postures/sitting.png', id: 'sitting', name: 'local.postures.sitting' },
  { img: 'systems/veil-rider/icons/postures/crouching.png', id: 'crouching', name: 'local.postures.crouching' },
  { img: 'systems/veil-rider/icons/postures/crawling.png', id: 'crawling', name: 'local.postures.crawling' },
  { img: 'systems/veil-rider/icons/postures/kneeling.png', id: 'kneeling', name: 'local.postures.kneeling' },
  { img: 'systems/veil-rider/icons/postures/lyingback.png', id: 'lyingback', name: 'local.postures.proneb' },
  { img: 'systems/veil-rider/icons/postures/lyingprone.png', id: 'lyingprone', name: 'local.postures.pronef' },
  { img: 'systems/veil-rider/icons/postures/sittingchair.png', id: 'sittingchair', name: 'local.postures.sitting' },
  { img: 'systems/veil-rider/icons/conditions/shock1.png', id: 'shock1', name: 'local.conditions.shock' },
  { img: 'systems/veil-rider/icons/conditions/shock2.png', id: 'shock2', name: 'local.conditions.shock' },
  { img: 'systems/veil-rider/icons/conditions/shock3.png', id: 'shock3', name: 'local.conditions.shock' },
  { img: 'systems/veil-rider/icons/conditions/shock4.png', id: 'shock4', name: 'local.conditions.shock' },
  { img: 'systems/veil-rider/icons/conditions/reeling.png', id: 'reeling', name: 'local.conditions.reeling' },
  { img: 'systems/veil-rider/icons/conditions/tired.png', id: 'tired', name: 'local.conditions.tired' },
  { img: 'systems/veil-rider/icons/conditions/collapse.png', id: 'collapse', name: 'local.conditions.collapse' },
  { img: 'systems/veil-rider/icons/conditions/unconscious.png', id: 'unconscious', name: 'local.conditions.unconscious' },
  { img: 'systems/veil-rider/icons/conditions/minus1xhp.png', id: 'minushp1', name: 'local.conditions.minushp' },
  { img: 'systems/veil-rider/icons/conditions/minus2xhp.png', id: 'minushp2', name: 'local.conditions.minushp' },
  { img: 'systems/veil-rider/icons/conditions/minus3xhp.png', id: 'minushp3', name: 'local.conditions.minushp' },
  { img: 'systems/veil-rider/icons/conditions/minus4xhp.png', id: 'minushp4', name: 'local.conditions.minushp' },
  { img: 'systems/veil-rider/icons/conditions/stunned.png', id: 'stunned', name: 'local.conditions.stunned' },
  { img: 'systems/veil-rider/icons/conditions/surprised.png', id: 'surprised', name: 'local.conditions.surprised' },
  { img: 'systems/veil-rider/icons/defeated.png', id: 'dead', name: 'local.conditions.defeated' },
  { img: 'systems/veil-rider/icons/blank.png', id: 'none', name: 'local.conditions.none' },
  { img: 'systems/veil-rider/icons/stances/hth.svg', id: 'hth', name: 'local.stances.hth' },
  { img: 'systems/veil-rider/icons/stances/magic.svg', id: 'magic', name: 'local.stances.magic' },
  { img: 'systems/veil-rider/icons/stances/ranged.svg', id: 'ranged', name: 'local.stances.ranged' },
  { img: 'systems/veil-rider/icons/stances/thrown.svg', id: 'thrown', name: 'local.stances.thrown' },
  { img: 'systems/veil-rider/icons/crippled/crippledleftarm.png', id: 'crippledleftarm', name: 'local.conditions.crippled' },
  { img: 'systems/veil-rider/icons/crippled/crippledlefthand.png', id: 'crippledlefthand', name: 'local.conditions.crippled' },
  { img: 'systems/veil-rider/icons/crippled/crippledleftleg.png', id: 'crippledleftleg', name: 'local.conditions.crippled' },
  { img: 'systems/veil-rider/icons/crippled/crippledleftfoot.png', id: 'crippledleftfoot', name: 'local.conditions.crippled' },
  { img: 'systems/veil-rider/icons/crippled/crippledrightarm.png', id: 'crippledrightarm', name: 'local.conditions.crippled' },
  { img: 'systems/veil-rider/icons/crippled/crippledrighthand.png', id: 'crippledrighthand', name: 'local.conditions.crippled' },
  { img: 'systems/veil-rider/icons/crippled/crippledrightleg.png', id: 'crippledrightleg', name: 'local.conditions.crippled' },
  { img: 'systems/veil-rider/icons/crippled/crippledrightfoot.png', id: 'crippledrightfoot', name: 'local.conditions.crippled' },
  { img: 'systems/veil-rider/icons/sizemods/smneg1.png', id: 'smaller1', name: 'local.conditions.smaller' },
  { img: 'systems/veil-rider/icons/sizemods/smneg2.png', id: 'smaller2', name: 'local.conditions.smaller' },
  { img: 'systems/veil-rider/icons/sizemods/smneg3.png', id: 'smaller3', name: 'local.conditions.smaller' },
  { img: 'systems/veil-rider/icons/sizemods/smneg4.png', id: 'smaller4', name: 'local.conditions.smaller' },
  { img: 'systems/veil-rider/icons/sizemods/smpos1.png', id: 'larger1', name: 'local.conditions.larger' },
  { img: 'systems/veil-rider/icons/sizemods/smpos2.png', id: 'larger2', name: 'local.conditions.larger' },
  { img: 'systems/veil-rider/icons/sizemods/smpos3.png', id: 'larger3', name: 'local.conditions.larger' },
  { img: 'systems/veil-rider/icons/sizemods/smpos4.png', id: 'larger4', name: 'local.conditions.larger' }
];

CONFIG.controlIcons.defeated = "systems/veil-rider/icons/defeated.png";

CONFIG.tokenEffects = {
  effectSize: {
    xLarge: 2,
    large: 3,
    medium: 4,
    small: 5
  },
  effectSizeChoices: {
    small: "Small (Default) - 5x5",
    medium: "Medium - 4x4",
    large: "Large - 3x3",
    xLarge: "Extra Large - 2x2"
  }
};
